#!/bin/bash
# BUILD script for demonstration purpose for HR Informatics

# Switch to app directory
cd app/

# Perform unit tests
./gradlew test

# Perform a build
./gradlew build

# Package application into runnable jar
./gradlew quarkusBuild --uber-jar

# Copy executable to the Docker build context
cp build/app-unspecified-runner.jar ../container/app-runner.jar

# Swith dir to root project dir
cd ../

# Build container with name quarkus-container
docker build -t quarkus-container  ./container